//
//  DogsTableViewController.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 13/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import UIKit

class DogsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //IBOutlets
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    //iVars
    let cellReuseIdentifier = "wxCell"
    
    private var dogs: [Dog]? {
        didSet {
            //reload table view data
            self.activityIndicator.stopAnimating()
            self.tableView.reloadData()
        }
    }

    // View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = Constants.navigationTitle
        self.activityIndicator.startAnimating()
        self.tableView.register(WXHomeworkCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)
        self.fetchData()
    }
    
    // Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dogs = self.dogs {
            return dogs.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row)")
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // Table View DataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: self.cellReuseIdentifier) as! WXHomeworkCell
        var text = "Name not found"
        
        if let dogs = self.dogs {
            if(indexPath.row < dogs.count &&
                dogs[indexPath.row].breed?.name != nil) {
                text = dogs[indexPath.row].breed!.name!
            }
        }
        
        cell.textLabel?.text = text
        
        return cell
    }
    
    // Private
    
    // Fetch Data
    private func fetchData() {
        
        let layer = ServiceLayer.init()
        
        layer.fetchData() { dogs, error in
            if error != nil {
                //show alert
                self.showAlert(error!)
            } else {
                if let allDogs = dogs {
                    //handle data
                    self.dogs = allDogs
                }
            }
        }
    }
    
    // Show Alert
    private func showAlert(_ error: WXError) {
        print (error)
    }
}

