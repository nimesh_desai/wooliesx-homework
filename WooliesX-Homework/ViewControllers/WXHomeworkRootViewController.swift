//
//  WXHomeworkRootViewController.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 13/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import UIKit

class WXHomeworkRootViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // Constants
    private struct Constants {
        struct Error {
            static let errorDomain = "com.x.woolies.WXHomeworkRootViewController"
        }
        
        struct Cell {
            static let cellReuseIdentifier = "wxHomeworkCell"
        }
        
        struct Navigation {
            static let navigationTitle = "WooliesX-Homework"
            static let sortButtonTitleAscending = "LifeSpan \u{2191}"
            static let sortButtonTitleDescending = "LifeSpan \u{2193}"
        }
        
        struct Alert {
            static let alertTitle = "Error"
            static let alertCancelButtonText = "OK"
            static let alertActionButtonText = "Retry"
        }
    }
    
    //IBOutlets
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var tableView: UITableView!
    
    //iVars
    let dataSourceHelper = DataSourceHelper()
    
    private var dogsDataSource: [DogDataSource]? {
        didSet {
            //reload table view data
            DispatchQueue.main.async {
                self.stopRefreshAnimation()
                self.tableView.reloadData()
            }
        }
    }

    // View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupNavigatorBar()
        self.setupPullToRefresh()
        self.activityIndicator.startAnimating()
        self.fetchData()
    }
    
    // Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dogs = self.dogsDataSource {
            return dogs.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // Table View DataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: Constants.Cell.cellReuseIdentifier,
                                                      for: indexPath) as! WXHomeworkCell
        
        if let dogs = self.dogsDataSource {
            if(indexPath.row < dogs.count) {
                cell.updateCellWithDog(dogs[indexPath.row])
            }
        }
        
        return cell
    }
    
    // Private
    
    // Setup Navigation Bar
    private func setupNavigatorBar() {
        self.title = Constants.Navigation.navigationTitle
        let sortButton = UIBarButtonItem(title: Constants.Navigation.sortButtonTitleAscending,
                                         style: .done,
                                         target: self,
                                         action: #selector(sortButtonTapped(_:)))
        self.navigationItem.rightBarButtonItem = sortButton
    }
    
    private func updateNavigationTitle(_ forAscending: Bool) {
        let title = forAscending ? Constants.Navigation.sortButtonTitleAscending : Constants.Navigation.sortButtonTitleDescending
        
        if let sortButton = self.navigationItem.rightBarButtonItem {
            sortButton.title = title
        }
    }
    
    // Sort Button Tap
    @objc private func sortButtonTapped(_ sender: Any) {
        if var dogs = self.dogsDataSource {
            self.dataSourceHelper.toggleSort(&dogs)
            self.dogsDataSource = dogs
            self.updateNavigationTitle(self.dataSourceHelper.isSortedInAscending)
        }
    }
    
    // Setup Pull To Refresh
    private func setupPullToRefresh() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self,
                                 action: #selector(refreshData(_:)),
                                 for: .valueChanged)

        self.tableView.refreshControl = refreshControl
    }
    
    @objc private func refreshData(_ sender: Any) {
        self.fetchData()
    }
    
    // Fetch Data
    private func fetchData() {
        self.dataSourceHelper.fetchData() {[weak self] dogs, error  in
            guard let self = self else {
                return
            }
            
            if error != nil {
                //show alert
                self.showAlertForError(error!)
            } else {
                if let allDogs = dogs {
                    self.dogsDataSource = allDogs
                } else {
                    let wxError = WXError.getDefaultError(Constants.Error.errorDomain)
                    self.showAlertForError(wxError)
                }
            }
        }
    }
    
    // Show Alert
    private func showAlertForError(_ error: WXError) {
        
        DispatchQueue.main.async {
            self.stopRefreshAnimation()
            
            let alert = UIAlertController(title: Constants.Alert.alertTitle,
                                          message: error.description,
                                          preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: Constants.Alert.alertCancelButtonText,
                                             style: .cancel,
                                             handler: { action in
            })
            let retryAction = UIAlertAction(title: Constants.Alert.alertActionButtonText,
                                            style: .default,
                                            handler: { action in
                                                self.activityIndicator.startAnimating()
                                                self.fetchData()
            })
            alert.addAction(cancelAction)
            alert.addAction(retryAction)
        
            self.present(alert,
                         animated: true,
                         completion: nil)
        }
    }
    
    private func stopRefreshAnimation() {
        self.tableView.refreshControl?.endRefreshing()
        UIView.animate(withDuration: 0.1) {
            self.tableView.contentOffset = CGPoint.zero
        }
        self.activityIndicator.stopAnimating()
    }
}

