//
//  WXHomeworkCell.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 14/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import Foundation
import UIKit

class WXHomeworkCell : UITableViewCell {
    
    // IBOutlets
    @IBOutlet private weak var dogImageView: UIImageView!
    @IBOutlet private weak var dogDisplayNameLabel: UILabel!
    @IBOutlet private weak var dogLifespanLabel: UILabel!
    @IBOutlet private weak var dogWeightLabel: UILabel!
    @IBOutlet private weak var dogImageLoadingActivityIndicator: UIActivityIndicatorView!
    private weak var dogDataSource: Displayable?
    
    // Overrides
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.resetDogDataSource()
        self.dogImageLoadingActivityIndicator.startAnimating()
        self.dogImageView.image = nil
        self.dogDisplayNameLabel.text = nil
        self.dogLifespanLabel.text = nil
        self.dogWeightLabel.text = nil
    }
    
    // Public
    
    // Update Cell With Dog
    public func updateCellWithDog(_ dog: Displayable) {
        self.resetDogDataSource()
        
        self.dogDataSource = dog
        
        dog.getDisplayableImage({ [weak self] (image) in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async {
                self.dogImageView.image = image
                if (image != nil) {
                    self.dogImageLoadingActivityIndicator.stopAnimating()
                }
            }
        })
        self.dogDisplayNameLabel.text = dog.displayableName
        self.dogLifespanLabel.text = dog.displayableLifespan
        self.dogWeightLabel.text = dog.displayableWeight
    }
    
    private func resetDogDataSource() {
        if let dogDataSource = self.dogDataSource {
            dogDataSource.resetImageDownloadCompletionHandler()
            self.dogDataSource = nil
        }
    }
}
