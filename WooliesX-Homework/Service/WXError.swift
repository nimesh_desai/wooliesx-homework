//
//  Error.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 13/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import Foundation

struct WXError:Error {
    let domain: String
    let code: Int
    let description: String
    
    fileprivate init(_ domain: String,
                     _ code: Int,
                     _ description: String) {
        self.domain = domain
        self.code = code
        self.description = description
    }
}

extension WXError {

    private struct Constants {
        static let networkErrorCode = 4001
        static let networkErrorDescription = "An error occured while trying to download data. Please try again."
        static let networkConnectionErrorCode = 4002
        static let networkConnectionErrorDescription = "Could not connect to the servers. Please check your internet connection and try again."
        static let dataErrorCode = 4003
        static let dataErrorDescription = "An error occured while trying to parse your data. Please try again."
    }
    
    static func getNetworkError(_ domain: String) -> WXError {
        return WXError(domain,
                       Constants.networkErrorCode,
                       Constants.networkErrorDescription)
    }
    
    static func getInternetNotReachableError(_ domain: String) -> WXError {
        return WXError(domain,
                       Constants.networkConnectionErrorCode,
                       Constants.networkConnectionErrorDescription)
    }
    
    static func getDataError(_ domain: String) -> WXError {
        return WXError(domain,
                       Constants.dataErrorCode,
                       Constants.dataErrorDescription)
    }
    
    static func getDefaultError(_ domain: String) -> WXError {
        return WXError(domain,
                       Constants.networkErrorCode,
                       Constants.networkErrorDescription)
    }
}
