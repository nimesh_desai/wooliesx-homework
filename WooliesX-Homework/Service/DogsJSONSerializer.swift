//
//  JSONSerializer.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 15/10/20.
//  Copyright © 2020 WooliesX. All rights reserved.
//

import Foundation

class DogsJSONSerializer {
    
    public static func generateDogsDataFrom(_ jsonData: Data) -> [Dog]? {
        do {
            let dogs = try JSONDecoder().decode([Dog].self,
                                                from: jsonData)
            return dogs
        } catch {
            return nil
        }
    }
}
