//
//  ServiceLayer.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 13/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import Foundation
import Alamofire

class ServiceLayer {
    
    private struct Constants {
        struct API {
            static let URL = "https://api.thedogapi.com/v1/images/search?limit=50"
        }
        
        struct Error {
            static let errorDomain = "com.x.woolies.serviceLayer"
        }
    }
    
    private var isNetworkReachable: Bool {
        get {
            if let manager = NetworkReachabilityManager() {
                return manager.isReachable
            }
            
            return false
        }
    }
    
    //Public
    
    // Fetch Data
    
    // I have decided to use Alamofire to display my skills using Cocoapods and Alamofire.
    // Data and image fetch methods can also be easily implemented using URLSession, something along the lines as shown below:
    
    public func fetchData_Without_AF(_ completion: @escaping (_ dogs: [Dog]?, _ error: WXError?) -> Void) {
        let url = URL(string: Constants.API.URL)

        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard error == nil else {
                let urlError = error as? URLError
                let wxError = self.getWXErrorForUrlError(urlError)
                completion(nil, wxError)
                return
            }

            guard let dogsData = data else {
                let error = WXError.getDefaultError(Constants.Error.errorDomain)
                completion(nil, error)
                return
            }

            guard let dogs = DogsJSONSerializer.generateDogsDataFrom(dogsData) else {
                let error = WXError.getDefaultError(Constants.Error.errorDomain)
                completion(nil, error)
                
                return
            }
            
            completion(dogs, nil)
        }

        task.resume()
    }
    
    public func fetchData_With_AF(_ completion: @escaping (Result<[Dog], WXError>) -> Void) {
        
        if self.isNetworkReachable {
            AF.request(Constants.API.URL, method: .get).response(queue: DispatchQueue.global(qos: .background)) { (response) in
                switch response.result {
                    
                case .success(_) :
                    
                    if let dogsData = response.value ?? nil {
                        if let dogs = DogsJSONSerializer.generateDogsDataFrom(dogsData) {
                            completion(.success(dogs))
                        } else {
                            //create Error obj - throw unable to parse data error
                            let error = WXError.getDataError(Constants.Error.errorDomain)
                            completion (.failure(error))
                            
                        }
                    } else {
                        //create Error obj - throw unable to parse data error
                        let error = WXError.getDataError(Constants.Error.errorDomain)
                        completion (.failure(error))
                    }
                    
                
                case .failure(let afError) :
                    
                    let error = self.getWXErrorForUrlError(afError.underlyingError as? URLError)
                    completion (.failure(error))
                }
            }
        } else {
            //create Error obj - throw network error
            let error = WXError.getNetworkError(Constants.Error.errorDomain)
            completion (.failure(error))
        }
    }
    
    //Fetch Image
    public func fetchImageFromURLString(_ urlString: String,
                                        _ completion: @escaping (_ image: UIImage?, _ error: WXError?) -> Void) {
        if self.isNetworkReachable {
            AF.request(urlString, method: .get).response(queue: DispatchQueue.global(qos: .background)) { response in
                 switch response.result {
                 case .success(let responseData):
                     let image = UIImage(data: responseData!, scale:1)
                     completion(image, nil)
                 
                 case .failure(let afError):

                     let error = self.getWXErrorForUrlError(afError.underlyingError as? URLError)
                     completion(nil, error)
                 }
             }
        } else {
            let error = WXError.getInternetNotReachableError(Constants.Error.errorDomain)
            completion(nil, error)
        }
    }
    
    // Generate error from URLError
    private func getWXErrorForUrlError(_ urlError: URLError?) -> WXError {
        if let error = urlError {
            switch error.code {
                case .cannotFindHost :
                    return WXError.getNetworkError(Constants.Error.errorDomain)
                
                case .notConnectedToInternet:
                    return WXError.getInternetNotReachableError(Constants.Error.errorDomain)
                
                default:
                    return WXError.getDefaultError(Constants.Error.errorDomain)
            }
        }
        
        return WXError.getNetworkError(Constants.Error.errorDomain)
    }
}
