//
//  DogSorter.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 14/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import Foundation

class DogSorter {
    // Public
    public func sort(_ d1: MinMaxLifeSpansForSorting,
                     _ d2: MinMaxLifeSpansForSorting,
                     _ state: SortState) -> Bool {
        
        //true means d1 should be before d2 in the sorted array
        //false otherwise
        
        //if max are the same, fallback on min
        if (d1.maxLifeSpan == d2.maxLifeSpan) {
            if (state == .ascending) {
                return d1.minLifeSpan <= d2.minLifeSpan
            } else {
                return d1.minLifeSpan >= d2.minLifeSpan
            }
        }
        
        //else sort on max
        if (state == .ascending) {
            return (d1.maxLifeSpan <= d2.maxLifeSpan)
        } else {
            return (d1.maxLifeSpan >= d2.maxLifeSpan)
        }
    }
}
