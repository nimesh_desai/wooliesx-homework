//
//  String+MinMaxLifeSpan.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 14/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import Foundation

extension String {
    func getMinAndMaxLifeSpanFromlifeSpan() -> (UInt, UInt) {
        if (self.count == 0) {
            return (0, 0)
        }
        
        var minLifeSpan = Int(Int32.max)
        var maxLifeSpan = Int(Int32.min)
        
        let stringArray = self.components(separatedBy: CharacterSet.decimalDigits.inverted)
        for item in stringArray {
            if let intValue = Int(item) {
                //check with min
                if (intValue < minLifeSpan) {
                    minLifeSpan = intValue
                }
                
                //check with max as well
                if intValue > maxLifeSpan {
                    maxLifeSpan = intValue
                }
            }
        }
        
        if (minLifeSpan == Int(Int32.max)) {
            minLifeSpan = 0
        }
        
        if (maxLifeSpan == Int(Int32.min)) {
            maxLifeSpan = 0
        }
        
        return (UInt(minLifeSpan), UInt(maxLifeSpan))
    }
}
