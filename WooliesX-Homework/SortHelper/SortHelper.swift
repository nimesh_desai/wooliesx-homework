//
//  SortHelper.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 14/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import Foundation

enum SortState {
    case ascending
    case descending
}

class SortHelper {
    // iVars
    private(set) var sortState:SortState = .ascending
    
    //Public
    
    //Sort Ascending
    public func sortInAscending(_ dogs: inout [DogDataSource]) {
        if (dogs.count <= 1) {
            return
        }
        
        self.sortState = .ascending
        self.sort(&dogs)
    }
    
    //Sort Descending
    public func sortInDescending(_ dogs: inout [DogDataSource]) {
        if (dogs.count <= 1) {
            return
        }
        
        self.sortState = .descending
        self.sort(&dogs)
    }

    // Toggle Sort
    public func toggleSort(_ dogs: inout [DogDataSource]) {
        if (dogs.count <= 1) {
            return
        }
        
        if (self.sortState == .ascending) {
            self.sortState = .descending
        } else {
            self.sortState = .ascending
        }
        self.sort(&dogs)
    }
    
    //Private
    private func sort( _ dogs: inout [DogDataSource]) {
        if (dogs.count <= 1) {
            return
        }

        let sorter = DogSorter()
        
        dogs.sort { (d1, d2) -> Bool in
            return sorter.sort(d1.minMaxLifeSpansForSorting, d2.minMaxLifeSpansForSorting, self.sortState)
        }
    }
}
