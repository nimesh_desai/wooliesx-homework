//
//  DataSourceHelper.swift
//  WooliesX-Homework
//
//  Created by Fifty on 14/10/20.
//  Copyright © 2020 WooliesX. All rights reserved.
//

import Foundation

class DataSourceHelper {
    
    private struct Constants {
        static let errorDomain = "com.x.woolies.DataSourceHelper"
    }
    
    private var dogsDataSource: [DogDataSource]?
    private lazy var sortHelper = SortHelper()
    
    // Public
    public func fetchData(_ completion: @escaping (_ dogs: [DogDataSource]?, _ error: WXError?) -> Void) {
        let layer = ServiceLayer.init()
        
        layer.fetchData() { dogsArray, error in
            if error != nil {
                completion(nil, error)
            } else {
                // generate DogsDataSource
                if let dogs = dogsArray {
                    
                    //generate datasource
                    var dogsDataSource = self.generateDogDataSource(dogs)

                    //sort in ascending
                    self.sortHelper.sortInAscending(&dogsDataSource)
                    
                    //return the datasource
                    completion(dogsDataSource, nil)
                } else {
                    let error = WXError.getNetworkError(Constants.errorDomain)
                    completion(nil, error)
                }
            }
        }
    }
    
    // Toggle Sort
    public func toggleSort(_ dogsDataSource: inout [DogDataSource]) {
        self.sortHelper.toggleSort(&dogsDataSource)
    }
    
    // Private
    private func generateDogDataSource(_ dogs: [Dog]) -> [DogDataSource] {
        var dataSource = [DogDataSource]()
        
        for dog in dogs {
            let dogDataSource = DogDataSource.init(dog)
            dataSource.append(dogDataSource)
        }
        
        return dataSource
    }
}
