//
//  DogsDataSource.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 14/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import Foundation
import UIKit

struct MinMaxLifeSpansForSorting {
    let minLifeSpan: UInt
    let maxLifeSpan: UInt
    
    init(_ min: UInt = 0,
         _ max: UInt = 0) {
        self.minLifeSpan = min
        self.maxLifeSpan = max
    }
    
    static var zero: MinMaxLifeSpansForSorting {
        get {
            return MinMaxLifeSpansForSorting()
        }
    }
}

class DogDataSource {
    enum ImageDownloadingState {
        case downloading
        case idle
    }
    
    private var dog: Dog
    private var dogImage: UIImage?
    private var imageDownloadingState: ImageDownloadingState = .idle
    private var imageDownlodedCompletionHandler:((UIImage?) -> Void)?
    
    private(set) var minMaxLifeSpansForSorting = MinMaxLifeSpansForSorting.zero
    
    // Public
    init(_ dog: Dog) {
        self.dog = dog
        self.setMinMaxLifeSpansForSorting()
        self.resetImageDownloadCompletionHandler()
    }
    
    // Private
    private func setMinMaxLifeSpansForSorting() {
        var minMaxLifeSpans: MinMaxLifeSpansForSorting
        
        if let lifeSpan = self.dog.lifeSpan {
            let result = lifeSpan.getMinAndMaxLifeSpanFromlifeSpan()
            minMaxLifeSpans = MinMaxLifeSpansForSorting(result.0, result.1)
        } else {
            minMaxLifeSpans = MinMaxLifeSpansForSorting.zero
        }
    
        self.minMaxLifeSpansForSorting = minMaxLifeSpans
    }
    
    private func fetchImageFromServer(_ completion: @escaping (_ image: UIImage?) -> Void) {
        if (self.imageDownloadingState == .downloading) {
            return 
        }
        
        guard let url = self.dog.url else {
            completion(nil)
            return
        }
        
        self.imageDownloadingState = .downloading
        weak var weakSelf = self
        
        let layer = ServiceLayer()
        layer.fetchImageFromURLString(url, { (image, error) in
            weakSelf?.imageDownloadingState = .idle
            weakSelf?.dogImage = image
            completion(image)
        })
    }
}

// Displayable Protocol Conformance Extension
extension DogDataSource: Displayable {
    
    struct Constants {
        static let defaultDisplayableID = "ID"
        static let defaultDisplayableLifespan = "Lifespan unavailable"
        static let defaultDisplayableWeight = "Weight unavailable"
        static let displayableMetricWeightUnits = "kg"
    }
    
    var displayableName: String {
        guard let name = self.dog.breed?.name else {
            //NOTE: when breed info is unavailable, we display id
            //can be changed according to business rules
            var id = ""
            if self.dog.id != nil {
                id = self.dog.id!
            }
            return Constants.defaultDisplayableID + ": \(id)"
        }
        
        return name
    }
    
    var displayableLifespan: String {
        guard let lifespan = self.dog.breed?.lifeSpan else {
            return Constants.defaultDisplayableLifespan
        }
        
        return lifespan
    }
    
    var displayableWeight: String {
        guard let weight = self.dog.breed?.weight?.metric else {
            return Constants.defaultDisplayableWeight
        }
        
        return weight + " \(Constants.displayableMetricWeightUnits)"
    }
    
    func getDisplayableImage(_ completion: @escaping (_ image: UIImage?) -> Void) {
        
        if let dogImage = self.dogImage {
            completion(dogImage)
            self.imageDownlodedCompletionHandler = nil
        } else {
            self.imageDownlodedCompletionHandler = completion
            weak var weakSelf = self
            
            self.fetchImageFromServer ({ (image) in
                weakSelf?.imageDownlodedCompletionHandler?(image)
                weakSelf?.imageDownlodedCompletionHandler = nil
            })
        }
    }
    
    func resetImageDownloadCompletionHandler() {
        self.imageDownlodedCompletionHandler = nil
    }
}
