//
//  DataSourceHelper.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 14/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import Foundation

class DataSourceHelper {
    
    private struct Constants {
        static let errorDomain = "com.x.woolies.DataSourceHelper"
    }
    
    private var dogsDataSource: [DogDataSource]?
    private lazy var sortHelper = SortHelper()
    
    var isSortedInAscending:Bool {
        get {
            return self.sortHelper.sortState == .ascending
        }
    }
    
    // Public
    public func fetchData(_ completion: @escaping (_ dogs: [DogDataSource]?, _ error: WXError?) -> Void) {
        let layer = ServiceLayer.init()
    
        layer.fetchData_With_AF() { result in
            switch (result) {
            case .success(let dogs):
                // generate DogsDataSource
                var dogsDataSource = self.generateDogDataSource(dogs)

                //sort in ascending
                self.sortHelper.sortInAscending(&dogsDataSource)
                
                //return the datasource
                completion(dogsDataSource, nil)
                
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    // Toggle Sort
    public func toggleSort(_ dogsDataSource: inout [DogDataSource]) {
        if (dogsDataSource.count <= 1) {
            return
        }
        
        self.sortHelper.toggleSort(&dogsDataSource)
    }
    
    // Private
    private func generateDogDataSource(_ dogs: [Dog]) -> [DogDataSource] {
        var dataSource = [DogDataSource]()
        
        for dog in dogs {
            //NOTE: Here I am choosing to leave in all dogs, includining ones whose [Breed] array has 0 items
            //based on business requirements, we could ignore those
            let dogDataSource = DogDataSource.init(dog)
            dataSource.append(dogDataSource)
        }
        
        return dataSource
    }
}
