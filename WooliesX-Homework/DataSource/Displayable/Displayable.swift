//
//  Displayable.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 14/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import Foundation
import UIKit

protocol Displayable : AnyObject {
    var displayableName: String { get }
    var displayableLifespan: String { get }
    var displayableWeight: String { get }
    func getDisplayableImage(_ completion: @escaping (_ image: UIImage?) -> Void)
    func resetImageDownloadCompletionHandler()
}
