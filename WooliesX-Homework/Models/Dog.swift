//
//  Dog.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 13/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import Foundation

struct Dog : Decodable {
    
    let id: String?
    let url: String?
    let width: Int?
    let height: Int?
    let breeds: [Breed]?
    
    //Computed Properties
    var breed : Breed? {
        //hardcoding to first object right now
        //based on business rules, this logic can be updated
        get {
            if let breeds = self.breeds {
                return breeds.count > 0 ? breeds[0] : nil
            }
            
            return nil
        }
    }
    
    //Helps in sorting
    var lifeSpan: String? {
        get {
            if let breed = self.breed {
                return breed.lifeSpan
            }
            
            return nil
        }
    }
    
    init (id: String? = nil,
          url: String? = nil,
          width: Int? = nil,
          height: Int? = nil,
          breeds: [Breed]? = nil) {
        self.id = id
        self.url = url
        self.width = width
        self.height = height
        self.breeds = breeds
    }

    enum CodingKeys: String, CodingKey {
        case id
        case url
        case width
        case height
        case breeds
    }
}
