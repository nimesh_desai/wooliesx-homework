//
//  Breed.swift
//  WooliesX-Homework
//
//  Created by Nimesh 'Fifty' Desai on 13/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import Foundation

struct Breed: Decodable {
    
   struct Weight : Decodable {
        let imperial: String?
        let metric: String?
        
        enum CodingKeys: String, CodingKey {
            case imperial
            case metric
        }
    }
    
    struct Height : Decodable {
        let imperial: String?
        let metric: String?
        
        enum CodingKeys: String, CodingKey {
            case imperial
            case metric
        }
    }
    
    let weight: Weight?
    let height: Height?
    let id: Int?
    let name: String?
    let countryCode: String?
    let bredFor: String?
    let breedGroup: String?
    let lifeSpan: String?
    let temperament: String?
    
    // Public
    init(weight: Weight? = nil,
         height: Height? = nil,
         id: Int? = nil,
         name: String? = nil,
         countryCode: String? = nil,
         bredFor: String? = nil,
         breedGroup: String? = nil,
         lifeSpan: String? = nil,
         temperament: String? = nil) {
        self.weight = weight
        self.height = height
        self.id = id
        self.name = name
        self.countryCode = countryCode
        self.bredFor = bredFor
        self.breedGroup = breedGroup
        self.lifeSpan = lifeSpan
        self.temperament = temperament
    }

    //One option is to have a single enum of ALL coding keys throughout the business logic in one giant enum
    //another option is to separate them out as I have here (Breed and Dog each having their own enums)
    //Both have their advantages and disdvantages.
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case countryCode = "country_code"
        case bredFor = "bred_for"
        case breedGroup = "breed_group"
        case lifeSpan = "life_span"
        case temperament
        case weight
        case height
    }
}
