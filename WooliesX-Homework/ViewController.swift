//
//  RootViewController.swift
//  WooliesX-Homework
//
//  Created by Fifty on 13/10/20.
//  Copyright © 2020 WooliesX. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "WooliesX-Homework"
    }
}

