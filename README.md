## Description

To download and run the code, please follow the following instructions:

1. Download the source (either as a git repo or zip file).
2. Ensure you have Cocoapods installed, otherwise navigate to https://cocoapods.org/ and follow instructions to install it.
3. Navigate to the root directory of this project in Terminal and run 'pod install'.
4. After cocopods does its thing, you should have a 'WooliesX-Homework.xcworkspace' file in the root folder. Use that to launch Xcode.
5. Select the 'WooliesX-Homework` target and run.

---
## Design Decisions

1. I have decided to use Alamofire to demonstrate my skills using Cocoapods and Alamofire. Data and image downloads can also be easily implemented using URLSession. An example is shown in ServiceLayer.swift. See line #37, public func fetchData_Without_AF() method.
2. I have deciced to show Dogs who don't have "Breed" info. Depending on business rules, such objects can be disdcarded in a production app.
