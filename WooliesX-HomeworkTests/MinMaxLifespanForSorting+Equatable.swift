//
//  MinMaxLifespanForSorting_Equatable.swift
//  WooliesX-HomeworkTests
//
//  Created by Fifty on 15/10/20.
//  Copyright © 2020 WooliesX. All rights reserved.
//

extension MinMaxLifeSpansForSorting : Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        if (lhs.minLifeSpan != rhs.minLifeSpan) {
            return false
        }
        
        if (lhs.maxLifeSpan != rhs.maxLifeSpan) {
            return false
        }
        
        return true
    }
}
