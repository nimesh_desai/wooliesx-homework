//
//  DogDataSourceTests.swift
//  WooliesX-HomeworkTests
//
//  Created by Nimesh 'Fifty' Desai on 15/10/20.
//  Copyright © 2020 WooliesX. All rights reserved.
//

import XCTest

class WooliesX_DogDataSource_Tests: XCTestCase {
    
    func test_min_max_lifespan_computed_property_when_breed_lifespan_valid_min_max_min_less_than_max() {
        let min: UInt = 7
        let max: UInt = 11
        let lifeSpan = "\(min) - \(max) years"
        let d1 = generateTestDogDataSourceFrom(lifeSpan)
        
        let result = d1.minMaxLifeSpansForSorting
        
        let expected = MinMaxLifeSpansForSorting(min, max)
        
        XCTAssertEqual(result, expected, "Expected result is \(expected), but we got \(result)")
    }
    
    func test_min_max_lifespan_computed_property_when_breed_lifespan_valid_min_max_min_greater_than_max() {
        let min: UInt = 17
        let max: UInt = 11
        let lifeSpan = "\(min) - \(max) years"
        let d1 = generateTestDogDataSourceFrom(lifeSpan)
        
        let result = d1.minMaxLifeSpansForSorting
        let expected = MinMaxLifeSpansForSorting(max, min)
        
        XCTAssertEqual(result, expected, "Expected result is \(expected), but we got \(result)")
    }
    
    func test_min_max_lifespan_computed_property_when_breed_lifespan_valid_min_max_equal() {
        let min: UInt = 17
        let max: UInt = 17
        let lifeSpan = "\(min) - \(max) years"
        let d1 = generateTestDogDataSourceFrom(lifeSpan)
        
        let result = d1.minMaxLifeSpansForSorting
        let expected = MinMaxLifeSpansForSorting(min, max)
        
        XCTAssertEqual(result, expected, "Expected result is \(expected), but we got \(result)")
    }
    
    func test_min_max_lifespan_computed_property_when_breed_lifespan_single_lifespan() {
        let lifeSpan = "17 years"
        let d1 = generateTestDogDataSourceFrom(lifeSpan)
        
        let result = d1.minMaxLifeSpansForSorting
        let expected = MinMaxLifeSpansForSorting(17, 17)
        
        XCTAssertEqual(result, expected, "Expected result is \(expected), but we got \(result)")
    }
    
    func test_min_max_lifespan_computed_property_when_breed_lifespan_contains_no_lifespan() {
        let d1 = generateTestDogDataSourceFrom("years")
        
        let result = d1.minMaxLifeSpansForSorting
        let expected = MinMaxLifeSpansForSorting.zero
        
        XCTAssertEqual(result, expected, "Expected result is \(expected), but we got \(result)")
    }
    
    func test_min_max_lifespan_computed_property_when_breed_lifespan_nil() {
        let d1 = generateTestDogDataSourceFrom(nil)
        
        let result = d1.minMaxLifeSpansForSorting
        let expected = MinMaxLifeSpansForSorting.zero
        
        XCTAssertEqual(result, expected, "Expected result is \(expected), but we got \(result)")
    }
    
    private func generateTestDogDataSourceFrom(_ lifeSpan: String?) -> DogDataSource {
        let breeds = Breed(lifeSpan:lifeSpan)
        let dog = Dog(id: "",
                      url: "",
                      width: 0,
                      height: 0,
                      breeds: [breeds])
        
        let dataSource = DogDataSource.init(dog)
        
        return dataSource
    }
}
