//
//  WooliesX_ServiceLayer_Tests.swift
//  WooliesX-HomeworkTests
//
//  Created by Fifty on 15/10/20.
//  Copyright © 2020 WooliesX. All rights reserved.
//

import XCTest

class WooliesX_DogJSONSerializer_Tests: XCTestCase {
    
    func test_data_generation_when_valid_data() {
        guard let data = self.getJSONDataFromFile("valid_one_dog") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let dogs = DogsJSONSerializer.generateDogsDataFrom(data) else {
            XCTFail("dogs should not be nil")
            return
        }
        
        let expectedID = "SkFt1gc47"
        let expectedLifeSpan = "8 - 15 years"
        let expectedURL = "https://cdn2.thedogapi.com/images/SkFt1gc47_1280.jpg"
        let expectedWidth = 800
        let expectedHeight = 600
        
        XCTAssertEqual(dogs.count, 1, "dogs should have object at 0")
        XCTAssertEqual(dogs[0].id!, expectedID, "Expected ID is: \(expectedID), but got: \(dogs[0].id!)")
        XCTAssertEqual(dogs[0].lifeSpan!, expectedLifeSpan, "Expected lifespan is: \(expectedLifeSpan), but got: \(dogs[0].lifeSpan!)")
        XCTAssertEqual(dogs[0].url!, expectedURL, "Expected URL is: \(expectedURL), but got: \(dogs[0].url!)")
        XCTAssertEqual(dogs[0].width!, expectedWidth, "Expected width is: \(expectedWidth), but got: \(dogs[0].width!)")
        XCTAssertEqual(dogs[0].height!, expectedHeight, "Expected height is: \(expectedHeight), but got: \(dogs[0].height!)")
    }
    
    func test_data_generation_when_mising_height() {
        guard let data = self.getJSONDataFromFile("valid_one_dog_missing_height") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let dogs = DogsJSONSerializer.generateDogsDataFrom(data) else {
            XCTFail("dogs should not be nil")
            return
        }
        
        let expectedID = "SkFt1gc47"
        let expectedLifeSpan = "8 - 15 years"
        let expectedURL = "https://cdn2.thedogapi.com/images/SkFt1gc47_1280.jpg"
        let expectedWidth = 800
        
        XCTAssertEqual(dogs.count, 1, "dogs should have object at 0")
        XCTAssertEqual(dogs[0].id!, expectedID, "Expected ID is: \(expectedID), but got: \(dogs[0].id!)")
        XCTAssertEqual(dogs[0].lifeSpan!, expectedLifeSpan, "Expected lifespan is: \(expectedLifeSpan), but got: \(dogs[0].lifeSpan!)")
        XCTAssertEqual(dogs[0].url!, expectedURL, "Expected URL is: \(expectedURL), but got: \(dogs[0].url!)")
        XCTAssertEqual(dogs[0].width!, expectedWidth, "Expected width is: \(expectedWidth), but got: \(dogs[0].width!)")
        XCTAssertNil(dogs[0].height, "Expected height to be nil, but got: \(dogs[0].height!)")
    }
    
    func test_data_generation_when_mising_width() {
        guard let data = self.getJSONDataFromFile("valid_one_dog_missing_width") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let dogs = DogsJSONSerializer.generateDogsDataFrom(data) else {
            XCTFail("dogs should not be nil")
            return
        }
        
        let expectedID = "SkFt1gc47"
        let expectedLifeSpan = "8 - 15 years"
        let expectedURL = "https://cdn2.thedogapi.com/images/SkFt1gc47_1280.jpg"
        let expectedHeight = 600
        
        XCTAssertEqual(dogs.count, 1, "dogs should have object at 0")
        XCTAssertEqual(dogs[0].id!, expectedID, "Expected ID is: \(expectedID), but got: \(dogs[0].id!)")
        XCTAssertEqual(dogs[0].lifeSpan!, expectedLifeSpan, "Expected lifespan is: \(expectedLifeSpan), but got: \(dogs[0].lifeSpan!)")
        XCTAssertEqual(dogs[0].url!, expectedURL, "Expected URL is: \(expectedURL), but got: \(dogs[0].url!)")
        XCTAssertNil(dogs[0].width, "Expected width nil, but got: \(dogs[0].width!)")
        XCTAssertEqual(dogs[0].height!, expectedHeight, "Expected height is: \(expectedHeight), but got: \(dogs[0].height!)")
    }
    
    func test_data_generation_when_mising_url() {
        guard let data = self.getJSONDataFromFile("valid_one_dog_missing_url") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let dogs = DogsJSONSerializer.generateDogsDataFrom(data) else {
            XCTFail("dogs should not be nil")
            return
        }
        
        let expectedID = "SkFt1gc47"
        let expectedLifeSpan = "8 - 15 years"
        let expectedWidth = 800
        let expectedHeight = 600
        
        XCTAssertEqual(dogs.count, 1, "dogs should have object at 0")
        XCTAssertEqual(dogs[0].id!, expectedID, "Expected ID is: \(expectedID), but got: \(dogs[0].id!)")
        XCTAssertEqual(dogs[0].lifeSpan!, expectedLifeSpan, "Expected lifespan is: \(expectedLifeSpan), but got: \(dogs[0].lifeSpan!)")
        XCTAssertNil(dogs[0].url, "Expected URL is nil, but got: \(dogs[0].url!)")
        XCTAssertEqual(dogs[0].width!, expectedWidth, "Expected width is: \(expectedWidth), but got: \(dogs[0].width!)")
        XCTAssertEqual(dogs[0].height!, expectedHeight, "Expected height is: \(expectedHeight), but got: \(dogs[0].height!)")
    }
    
    func test_data_generation_when_mising_id() {
        guard let data = self.getJSONDataFromFile("valid_one_dog_missing_id") else {
            XCTFail("Data should not be nil")
            return
        }
        
        guard let dogs = DogsJSONSerializer.generateDogsDataFrom(data) else {
            XCTFail("dogs should not be nil")
            return
        }
        
        let expectedLifeSpan = "8 - 15 years"
        let expectedURL = "https://cdn2.thedogapi.com/images/SkFt1gc47_1280.jpg"
        let expectedWidth = 800
        let expectedHeight = 600
        
        XCTAssertEqual(dogs.count, 1, "dogs should have object at 0")
        XCTAssertNil(dogs[0].id, "Expected ID is nil, but got: \(dogs[0].id!)")
        XCTAssertEqual(dogs[0].lifeSpan!, expectedLifeSpan, "Expected lifespan is: \(expectedLifeSpan), but got: \(dogs[0].lifeSpan!)")
        XCTAssertEqual(dogs[0].url!, expectedURL, "Expected URL is: \(expectedURL), but got: \(dogs[0].url!)")
        XCTAssertEqual(dogs[0].width!, expectedWidth, "Expected width is: \(expectedWidth), but got: \(dogs[0].width!)")
        XCTAssertEqual(dogs[0].height!, expectedHeight, "Expected height is: \(expectedHeight), but got: \(dogs[0].height!)")
    }
    
    private func getJSONDataFromFile(_ file: String) -> Data? {
        let testBundle = Bundle(for: type(of: self))
        
        if let path = testBundle.path(forResource: file, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
              } catch {
                return nil
              }
        }
        
        return nil
    }
}
