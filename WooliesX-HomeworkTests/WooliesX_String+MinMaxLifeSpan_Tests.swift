//
//  WooliesX_String_MinMax_LifeSpanTests.swift
//  WooliesX-HomeworkTests
//
//  Created by Nimesh 'Fifty' Desai on 13/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import XCTest

class WooliesX_String_MinMaxLifeSpan_Tests: XCTestCase {

    func test_minMaxWhenBothPresentInTestString() throws {
        let testString = "10 - 15 years"
        
        let result = testString.getMinAndMaxLifeSpanFromlifeSpan()
        
        XCTAssertEqual(result.0, 10, "Min value should have been 10")
        XCTAssertEqual(result.1, 15, "Max value should have been 15")
    }

    func test_minMaxWhenSingleLifeSpanInTestString() throws {
        let testString = "15 years"
        
        let result = testString.getMinAndMaxLifeSpanFromlifeSpan()
        
        XCTAssertEqual(result.0, 15, "Min value should have been 15")
        XCTAssertEqual(result.1, 15, "Max value should have been 15")
    }

    func test_minMaxWhenNoLifeSpanInTestString() throws {
        let testString = "years"
        
        let result = testString.getMinAndMaxLifeSpanFromlifeSpan()
        
        XCTAssertEqual(result.0, 0, "Min value should have been 0")
        XCTAssertEqual(result.1, 0, "Max value should have been 0")
    }

    func test_minMaxWhenTestStringIsEmpty() throws {
        let testString = ""
        
        let result = testString.getMinAndMaxLifeSpanFromlifeSpan()
        
        XCTAssertEqual(result.0, 0, "Min value should have been 0")
        XCTAssertEqual(result.1, 0, "Max value should have been 0")
    }
}
