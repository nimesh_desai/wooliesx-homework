//
//  WooliesX_Dog_Sorter_Tests.swift
//  WooliesX-HomeworkTests
//
//  Created by Nimesh 'Fifty' Desai on 14/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import XCTest

class WooliesX_DogSorter_Tests: XCTestCase {
    
    var dogSorter: DogSorter!
    
    override func setUp() {
        super.setUp()
        self.dogSorter = DogSorter()
    }

    override func tearDown() {
        self.dogSorter = nil
        super.tearDown()
    }

    func test_sorting_d1_d2_min_max_lifespans_unequal_max_d1_smaller_in_ascending() throws {
        let d1LifeSpan: String = "10 - 12 years"
        let d2LifeSpan: String = "12 - 18 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)
        
        
        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 in list")
    }

    func test_sorting_d1_d2_min_max_lifespans_unequal_max_d1_smaller_in_descending() throws {
        let d1LifeSpan = "10 - 12 years"
        let d2LifeSpan = "12 - 18 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertFalse(result, "Expected result is false, d1 should be AFTER d2 list")
    }

    func test_sorting_d1_d2_min_max_lifespans_unequal_max_d1_greater_in_ascending() throws {
        let d1LifeSpan = "12 - 18 years"
        let d2LifeSpan = "10 - 12 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertFalse(result, "Expected result is true, d1 should be AFTER d2 in list")
    }

    func test_sorting_d1_d2_min_max_lifespans_unequal_max_d2_smaller_in_descending() throws {
        let d1LifeSpan = "12 - 18 years"
        let d2LifeSpan = "10 - 12 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 list")
    }

    func test_sorting_d1_d2_min_max_lifespans_equal_max_d1_smaller_in_ascending() throws {
        let d1LifeSpan = "10 - 14 years"
        let d2LifeSpan = "12 - 14 years"
        
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 in list")
    }

    func test_sorting_d1_d2_min_max_lifespans_equal_max_d1_smaller_in_descending() throws {
        let d1LifeSpan = "10 - 14 years"
        let d2LifeSpan = "12 - 14 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertFalse(result, "Expected result is false, d1 should be AFTER d2 in list")
    }

    func test_sorting_d1_d2_min_max_lifespans_equal_max_d2_smaller_in_ascending() throws {
        let d1LifeSpan = "12 - 14 years"
        let d2LifeSpan = "10 - 14 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertFalse(result, "Expected result is false, d1 should be AFTER d2 in list")
    }

    func test_sorting_d1_d2_min_max_lifespans_equal_max_d2_smaller_in_descending() throws {
        let d1LifeSpan = "12 - 14 years"
        let d2LifeSpan = "10 - 14 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 in list")
    }

    func test_sorting_d1_d2_min_max_lifespans_equal_min_max_in_ascending() throws {
        let d1LifeSpan = "1 - 8 years"
        let d2LifeSpan = "1 - 8 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 in list")
    }

    func test_sorting_d1_d2_min_max_lifespans_equal_min_max_in_descending() throws {
        let d1LifeSpan = "1 - 8 years"
        let d2LifeSpan = "1 - 8 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 in list. This one is a special case since d1 and d2 have equal values")
    }

    func test_sorting_d1_single_lifespan_d2_min_max_lifespan_d1_smaller_ascending() throws {
        let d1LifeSpan = "8 years"
        let d2LifeSpan = "6 - 10 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 in list")
    }

    func test_sorting_d1_single_lifespan_d2_min_max_lifespan_d1_smaller_descending() throws {
        let d1LifeSpan = "8 years"
        let d2LifeSpan = "6 - 10 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertFalse(result, "Expected result is false, d1 should be AFTER d2 in list")
    }

    func test_sorting_d1_single_lifespan_d2_min_max_lifespan_d2_smaller_ascending() throws {
        let d1LifeSpan = "8 years"
        let d2LifeSpan = "4 - 6 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertFalse(result, "Expected result is false, d1 should be AFTER d2 in list")
    }

    func test_sorting_d1_single_lifespan_d2_min_max_lifespan_d2_smaller_descending() throws {
        let d1LifeSpan = "8 years"
        let d2LifeSpan = "4 - 6 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssert(result, "Expected result is true, d1 should be BEFORE d2 in list")
    }

    func test_sorting_d2_single_lifespan_d1_min_max_lifespan_d1_smaller_ascending() throws {
        let d1LifeSpan = "8 - 10 years"
        let d2LifeSpan = "12 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 in list")
    }

    func test_sorting_d2_single_lifespan_d1_min_max_lifespan_d1_smaller_descending() throws {
        let d1LifeSpan = "8 - 10 years"
        let d2LifeSpan = "12 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertFalse(result, "Expected result is false, d1 should be AFTER d2 in list")
    }

    func test_sorting_d2_single_lifespan_d1_min_max_lifespan_d2_smaller_ascending() throws {
        let d1LifeSpan = "5 - 17 years"
        let d2LifeSpan = "10 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertFalse(result, "Expected result is false, d1 should be AFTER d2 in list")
    }

    func test_sorting_d2_single_lifespan_d1_min_max_lifespan_d2_smaller_descending() throws {
        let d1LifeSpan = "5 - 17 years"
        let d2LifeSpan = "12 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 in list")
    }

    func test_sorting_d1_d2_single_lifespan_d1_smaller_ascending() throws {
        let d1LifeSpan = "9 years"
        let d2LifeSpan = "11 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 in list")
    }

    func test_sorting_d1_d2_single_lifespan_d1_smaller_descending() throws {
        let d1LifeSpan = "9 years"
        let d2LifeSpan = "11 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertFalse(result, "Expected result is false, d1 should be AFTER d2 in list")
    }

    func test_sorting_d1_d2_single_lifespan_d2_smaller_ascending() throws {
        let d1LifeSpan = "14 years"
        let d2LifeSpan = "7 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertFalse(result, "Expected result is true, d1 should be AFTER d2 in list")
    }

    func test_sorting_d1_d2_single_lifespan_d2_smaller_descending() throws {
        let d1LifeSpan = "14 years"
        let d2LifeSpan = "7 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertTrue(result, "Expected result is false, d1 should be BEFORE d2 in list")
    }

    func test_sorting_d1_nil_ascending() throws {
        let d1LifeSpan:String? = nil
        let d2LifeSpan = "7 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 in list")
    }

    func test_sorting_d1_nil_descending() throws {
        let d1LifeSpan:String? = nil
        let d2LifeSpan = "7 years"
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertFalse(result, "Expected result is false, d1 should be AFTER d2 in list")
    }

    func test_sorting_d2_nil_ascending() throws {
        let d1LifeSpan = "7 - 11 years"
        let d2LifeSpan:String? = nil
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .ascending)
        XCTAssertFalse(result, "Expected result is false, d1 should be AFTER d2 in list")
    }

    func test_sorting_d2_nil_descending() throws {
        let d1LifeSpan = "7 - 11 years"
        let d2LifeSpan:String? = nil
        let d1 = generateTestDogDataSourceFrom(d1LifeSpan)
        let d2 = generateTestDogDataSourceFrom(d2LifeSpan)

        let result = self.dogSorter.sort(d1.minMaxLifeSpansForSorting,
                                         d2.minMaxLifeSpansForSorting,
                                         .descending)
        XCTAssertTrue(result, "Expected result is true, d1 should be BEFORE d2 in list")
    }
    
    private func generateTestDogDataSourceFrom(_ lifeSpan: String?) -> DogDataSource {
        let breeds = Breed(lifeSpan:lifeSpan)
        let dog = Dog(id: "",
                      url: "",
                      width: 0,
                      height: 0,
                      breeds: [breeds])
        
        let dataSource = DogDataSource.init(dog)
        
        return dataSource
    }
}
