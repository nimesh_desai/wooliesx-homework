//
//  WooliesX_SortHelper_Tests.swift
//  WooliesX-HomeworkTests
//
//  Created by Nimesh 'Fifty' Desai on 14/10/20.
//  Copyright © 2020 Nimesh 'Fifty' Desai. All rights reserved.
//

import XCTest

class WooliesX_SortHelper_Tests: XCTestCase {
    
    var sortHelper: SortHelper!
    
    override func setUp() {
        super.setUp()
        self.sortHelper = SortHelper()
    }

    override func tearDown() {
        self.sortHelper = nil
        super.tearDown()
    }
    
    func test_sort_ascending() {
        let d1 = generateTestDogDataSourceFrom("123",
                                               "123.url",
                                               0,
                                               0,
                                               "12 - 14 years")
        
        let d2 = generateTestDogDataSourceFrom("456",
                                               "456.url",
                                               0,
                                               0,
                                               "8 - 11 years")
        var array = [d1, d2]
        self.sortHelper.sortInAscending(&array)
        
        XCTAssertTrue(array[0] === d2, "Expected result is d2 being the first element")
    }
    
    func test_sort_descending() {
        let d1 = generateTestDogDataSourceFrom("456",
                                               "456.url",
                                               0,
                                               0,
                                               "8 - 11 years")
        
        let d2 = generateTestDogDataSourceFrom("123",
                                               "123.url",
                                               0,
                                               0,
                                               "12 - 14 years")
        var array = [d1, d2]
        self.sortHelper.sortInDescending(&array)
        
        XCTAssertTrue(array[0] === d2, "Expected result is d2 being the first element")
    }
    
    func test_toggle_sort() {
        let d1 = generateTestDogDataSourceFrom("456",
                                               "456.url",
                                               0,
                                               0,
                                               "8 - 11 years")
        
        let d2 = generateTestDogDataSourceFrom("123",
                                               "123.url",
                                               0,
                                               0,
                                               "12 - 14 years")
        var array = [d1, d2]
        self.sortHelper.sortInAscending(&array) //after this, because of the above tests, we expect array to hold [d1, d2]
        
        self.sortHelper.toggleSort(&array) //now we toggle, thus now we're in descending
        
        XCTAssertTrue(array[0] === d2, "Expected result is d2 being the first element")
    }
    
    private func generateTestDogDataSourceFrom(_ id: String,
                                               _ url: String,
                                               _ width: Int,
                                               _ height: Int,
                                               _ lifeSpan: String) -> DogDataSource {
        let breeds = Breed(lifeSpan:lifeSpan)
        let dog = Dog(id: id, url: url, width: width, height: height, breeds: [breeds])
        
        let dataSource = DogDataSource.init(dog)
        
        return dataSource
    }
}
